Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sasquatch
Source: https://github.com/onekey-sec/sasquatch

Files: *
Copyright: 2002-2022 Phillip Lougher <phillip@squashfs.org.uk>
License: GPL-2.0+

Files: squashfs-tools/LZMA/lzmadaptive/C/7zip/Compress/LZMA_C/*
Copyright: 1999-2021 Igor Pavlov
License: LGPL-2.1+

Files: squashfs-tools/LZMA/lzmadaptive/C/7zip/Compress/LZMA_Lib/ZLib.cpp
Copyright: 2005-2006 Oleg I. Vdovikin <oleg@cs.msu.su>
License: LGPL-2.1+

Files: debian/*
Copyright: 2022 Marton Illes <marton.illes@iot-inspector.com>
           2023 Sophie Brun <sophie@offensive-security.com>
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
